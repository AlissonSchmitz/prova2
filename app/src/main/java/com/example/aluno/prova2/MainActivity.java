package com.example.aluno.prova2;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;

import java.text.NumberFormat;
import java.text.ParseException;

public class MainActivity extends AppCompatActivity {

    SeekBar seekJuros;
    EditText vl_emprestimo;
    EditText vl_juros;
    EditText nr_meses;
    private static NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
    private static NumberFormat percentFormat  = NumberFormat.getPercentInstance();
    protected int meses;
    protected double valorEmprestimo;
    protected double txJuros;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        vl_emprestimo= (EditText) findViewById(R.id.vl_emprestimo) ;
        vl_juros= (EditText) findViewById(R.id.txt_valorJuros);
        nr_meses= (EditText) findViewById(R.id.nr_meses);
        seekJuros= (SeekBar) findViewById(R.id.seekBarJuros);
        percentFormat.setMinimumFractionDigits(2);

        seekJuros.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                double valor= seekBar.getProgress();
                vl_juros.setText((percentFormat.format(valor/1000).toString()));
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

    }

    public void apresentarListView(View view) throws ParseException {
        Financiamento f = null;
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        nf.setGroupingUsed(true);
        nf.setMinimumFractionDigits(2);

        this.meses=  1;
        this.valorEmprestimo=0.0;
        this.txJuros=0.0;
        try {
            meses = Integer.parseInt( this.nr_meses.getText().toString());
            valorEmprestimo= Double.parseDouble(vl_emprestimo.getText().toString());
            txJuros= percentFormat.parse(vl_juros.getText().toString()).doubleValue();
            Bundle params = new Bundle();
            params.putInt("meses",meses);
            params.putDouble("valorEmprestimo", valorEmprestimo);
            params.putDouble("txJuros",txJuros);
            Intent intencao = new Intent(this, FinanciamentoListView.class);
            startActivity(intencao);
            intencao.putExtras(params);
            startActivity(intencao);
        }catch (Exception e){
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle("ERRO");
            alertDialog.setMessage("Mes " + meses +"\nvalor: "+valorEmprestimo+"Juros"+txJuros);
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }
    }
}
