package com.example.aluno.prova2;

public  class Parcela {
    private int ordemParcela;
    private double juros;
    private double saldoDevedor;
    private double amortizacao;

    public Parcela() {

    }

    public Parcela(int ordemParcela, double juros ,double amortizacao, double saldoDevedor) {
        this.ordemParcela=ordemParcela;
        this.juros=juros;
        this.saldoDevedor=saldoDevedor;
        this.amortizacao = amortizacao;
    }

    public double getValorPrestacao(){
        return this.amortizacao+this.juros;
    }

    public int getOrdemParcela() {
        return ordemParcela;
    }

    public double getJuros() {
        return juros;
    }

    public double getSaldoDevedor() {
        return saldoDevedor;
    }
}
