package com.example.aluno.prova2;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

public class FinanciamentoAdapter extends ArrayAdapter<Parcela>{
    private static  final String TAG = "Parcela";

    private Context mContext;
    private  int mResource;
    public FinanciamentoAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Parcela> objects) {
        super(context, resource, objects);
        this.mContext=context;
        this.mResource=resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView,  @NonNull ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView= inflater.inflate(mResource, parent, false);

        TextView tvNumeroParcela= (TextView) convertView.findViewById(R.id.nr_mes);
        TextView tvValorParcela= (TextView) convertView.findViewById(R.id.vl_parcela);
        TextView tvValorJuros= (TextView) convertView.findViewById(R.id.vl_juros);
        TextView tvValorDividada= (TextView) convertView.findViewById(R.id.vl_saldo);

        NumberFormat nc = NumberFormat.getCurrencyInstance();
        tvNumeroParcela.setText(Integer.toString(getItem(position).getOrdemParcela()));
        tvValorParcela.setText(nc.format(getItem(position).getValorPrestacao()));
        tvValorJuros.setText(nc.format(getItem(position).getJuros()));
        tvValorDividada.setText(nc.format(getItem(position).getSaldoDevedor()));

        return  convertView;
    }
}