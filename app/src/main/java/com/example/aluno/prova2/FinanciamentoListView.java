package com.example.aluno.prova2;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.TextView;

import java.text.NumberFormat;

public class FinanciamentoListView extends AppCompatActivity {
    protected ListView listView;
    protected Financiamento financiamento;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_do_list_view);

        this.listView=findViewById(R.id.lista);

        Intent intent=getIntent();

        int meses = intent.getExtras().getInt("meses");
        double valorEmprestimo = intent.getExtras().getDouble("valorEmprestimo");
        double txJuros = intent.getExtras().getDouble("txJuros");

        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("TESTANDO AS VARIAVEIS ");
        alertDialog.setMessage("Mes " + meses +"\nvalor: "+valorEmprestimo+"\nJuros"+txJuros);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();

        Financiamento f =  new Financiamento(meses,valorEmprestimo,txJuros);

        FinanciamentoAdapter adapter = new FinanciamentoAdapter(this, R.layout.view_tabela_price, f.getParecelas());
        listView.setAdapter(adapter);
    }
}
